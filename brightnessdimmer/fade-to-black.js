/* =========================== fade_to_black.js =================================
# This file is part of Brightness Dimmer extension for Falkon
# Copyright (C) 2019 Zdravko Mitov <mitovz@mail.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ============================================================ */
function fadeToBlack(){
'use strict';

    var css = function(val){

    let time = new Date();
    let epoch = time.getTime();

    var style = `html:before {
    content: "";
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    position: fixed;
    z-index: ${epoch};
    pointer-events: none;
    background-color: rgba(6, 6, 6, ${val});
    }`;

    return style
    }

    var el = document.getElementById('fade-to-black');
    if (el){
        el.remove();
    }

    var style = document.createElement('style');
    style.id = 'fade-to-black';
    style.textContent = css(V_A_L);
    document.documentElement.appendChild(style);
}

fadeToBlack();
