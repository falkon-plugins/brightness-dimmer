# ============================================================
# Brightness Dimmer extension for Falkon
# Copyright (C) 2019 Zdravko Mitov <mitovz@mail.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ============================================================
import Falkon
from PySide2 import QtGui
from brightnessdimmer.brightness_dim_slider import SliderWidget


class BrightnessDimButton(Falkon.AbstractButtonInterface):
    slider_bar = None
    sbar_shown = False

    def __init__(self, db):
        super().__init__()
        self.db = db
        self.setIcon(QtGui.QIcon.fromTheme("preferences-desktop-screensaver"))
        self.setTitle("Adjust Brightness")
        self.setToolTip("Adjust the view less bright or distinct.")
        self.clicked.connect(self.on_clicked_cb)

    def id(self):
        return "brightness-dimmer-button"

    def name(self):
        return "Brightness dimmer button"

    def on_slider_bar_del_cb(self):
        self.slider_bar = None
        self.sbar_shown = False

    def on_tab_changed_cb(self, unused):
        if self.sbar_shown:
            self.slider_bar.close()
            self.sbar_shown = False

    def on_clicked_cb(self, unused):
        if not self.slider_bar:
            view = self.webView()
            layout = view.webTab().layout()
            self.slider_bar = SliderWidget(view, self.db)
            layout.insertWidget(0, self.slider_bar)
            self.slider_bar.destroyed.connect(self.on_slider_bar_del_cb)
            self.webViewChanged.connect(self.on_tab_changed_cb)
        if not self.sbar_shown:
            self.slider_bar.show()
            self.sbar_shown = True
        else:
            self.slider_bar.close()
            self.sbar_shown = False
