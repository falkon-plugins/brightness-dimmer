# ============================================================
# Brightness Dimmer extension for Falkon
# Copyright (C) 2019 Zdravko Mitov <mitovz@mail.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ============================================================
import Falkon
import os
from PySide2 import QtSql, QtWidgets

CONNECTION = "falkon_brightness_adjustement"


class DBConnection:
    db_path = None
    db = None

    def create_connection(self):
        self.sript_dir = os.path.dirname(__file__)
        QtSql.QSqlDatabase.removeDatabase(CONNECTION)
        self.db = QtSql.QSqlDatabase.addDatabase("QSQLITE", CONNECTION)
        self.db.setDatabaseName(os.path.join(self.db_path, "brightness_dim_settings.db"))
        if not self.db.open():
            QtWidgets.QMessageBox.critical(None, "Extension wil not be loaded",
                                           "Unable to establish a database connection.\n"
                                           "This extension needs SQLite support. Please read "
                                           "the Qt SQL documentation for information "
                                           "how to enable it.\n\nClick Cancel to exit!",
                                           QtWidgets.QMessageBox.Cancel, QtWidgets.QMessageBox.NoButton)
            return False

        query = QtSql.QSqlQuery(self.db.database(CONNECTION))
        query.prepare("CREATE TABLE IF NOT EXISTS brightness"
                      "(url TEXT NOT NULL, val INTEGER)")
        query.exec_()
        query.prepare("CREATE UNIQUE INDEX url_uniq_index ON brightness (url);")
        query.exec_()
        query.prepare("INSERT INTO 'brightness' (url, val) VALUES (?, ?)")
        query.addBindValue("default")
        query.addBindValue(0)
        query.exec_()
        return True

    def get_value_for_url(self, url):
        query = QtSql.QSqlQuery(self.db.database(CONNECTION))
        query.prepare("SELECT val FROM 'brightness' WHERE url = ?")
        query.addBindValue(url)
        query.exec_()
        if not query.next():
            return False
        return query.value("val")

    def add_value_for_url(self, url, val):
        query = QtSql.QSqlQuery(self.db.database(CONNECTION))
        query.prepare("INSERT OR REPLACE INTO 'brightness' (url, val) VALUES (?, ?)")
        query.addBindValue(url)
        query.addBindValue(val)
        query.exec_()

    def apply_value(self, page, val=None):
        applicable = None
        url = page.url().host()
        url_val = self.get_value_for_url(url)
        default = self.get_value_for_url("default")
        if val == 0:
            page.runJavaScript("(function() {"
                               "'use strict';"
                               "var el = document.getElementById('fade-to-black');"
                               "if (el){el.remove()};"
                               "})();"
                               )
            return
        elif (not val and not url_val and default > 0 and not url_val == -1):
            applicable = default
        elif val and val > 0:
            applicable = val
        elif url_val and url_val > 0:
            applicable = url_val
        if applicable:
            js_script = Falkon.QzTools.readAllFileContents(
                os.path.join(self.sript_dir, "fade-to-black.js")
            )
            js_script = js_script.replace("V_A_L", str(applicable / 100))
            page.runJavaScript(js_script)
