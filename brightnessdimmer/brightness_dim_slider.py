# ============================================================
# Brightness Dimmer extension for Falkon
# Copyright (C) 2019 Zdravko Mitov <mitovz@mail.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ============================================================
from PySide2 import QtWidgets, QtCore


class SliderWidget(QtWidgets.QWidget):
    current = {}

    def __init__(self, view, db):
        super().__init__(parent=view)
        self.view = view
        self.db = db
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.label = QtWidgets.QLabel()
        self.label.setFrameStyle(QtWidgets.QFrame.StyledPanel)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.view.loadFinished.connect(self.on_reset_slider)

        self.slider = QtWidgets.QSlider(orientation=QtCore.Qt.Horizontal, parent=self)
        self.slider.setMaximumWidth(444)
        self.slider.setPageStep(10)
        self.slider.setSingleStep(1)
        self.slider.setRange(0, 99)
        self.on_reset_slider()
        self.slider.valueChanged.connect(self.on_value_chnged)

        self.check_box = QtWidgets.QCheckBox("Set as default")
        self.check_box.setToolTip("If checked, the current value will be saved as default.")
        self.check_box.setChecked(False)

        dsbl_btn = QtWidgets.QPushButton("Desable on this host")
        dsbl_btn.clicked.connect(self.on_disable_on_page)
        dsbl_btn.setToolTip("Do not reduce brightness on this hos.")

        deft_btn = QtWidgets.QPushButton("Reset to default")
        deft_btn.clicked.connect(self.on_set_default)
        deft_btn.setToolTip("Reset to default (or host) value.")

        apply_btn = QtWidgets.QPushButton("Save for this host")
        apply_btn.clicked.connect(self.on_apply_value)
        apply_btn.setToolTip("Save a non default value for the current host.")

        close_btn = QtWidgets.QPushButton("Close")
        close_btn.clicked.connect(self.close)
        close_btn.setDefault(True)
        close_btn.setToolTip("Close")

        layout = QtWidgets.QHBoxLayout(self)
        layout.setSpacing(12)
        layout.setContentsMargins(12, 6, 12, 6)
        self.on_set_label(self.slider.value())
        layout.addWidget(self.check_box)
        layout.addWidget(self.label)
        layout.addWidget(self.slider)
        layout.addWidget(deft_btn)
        layout.addWidget(dsbl_btn)
        layout.addWidget(apply_btn)
        layout.addWidget(close_btn)

    def on_reset_slider(self):
        url = self.view.page().url().host()
        curr = self.db.get_value_for_url(url)
        default = self.db.get_value_for_url("default")
        if curr:
            self.slider.setValue(curr)
        elif default and default > 0:
            self.slider.setValue(default)

    def on_set_label(self, val):
        if val > 0:
            lbl = "reduced %d %%" % val
        else:
            lbl = "disabled"
        self.label.setText(lbl)

    def on_apply_value(self):
        url = self.view.page().url().host()
        val = self.slider.value()
        self.db.add_value_for_url(url, val)
        self.close()

    def on_value_chnged(self, val):
        page = self.view.page()
        host = page.url().host()
        self.on_set_label(val)
        self.db.apply_value(page, val)
        self.current[host] = val

    def on_disable_on_page(self):
        page = self.view.page()
        url = page.url().host()
        self.db.add_value_for_url(url, -1)
        self.db.apply_value(page, 0)
        self.slider.setValue(0)

    def on_set_default(self):
        val = self.db.get_value_for_url("default")
        page = self.view.page()
        url = page.url().host()
        curr = self.db.get_value_for_url(url)
        if curr:
            val = curr
            if val == -1:
                val = 0
        self.on_value_chnged(val)
        self.on_reset_slider()

    def showEvent(self, event):
        host = self.view.page().url().host()
        if host in self.current:
            val = self.current[host]
            self.slider.setValue(val)
        event.accept()

    def closeEvent(self, event):
        if self.check_box.isChecked():
            self.db.add_value_for_url("default", self.slider.value())
        event.accept()
