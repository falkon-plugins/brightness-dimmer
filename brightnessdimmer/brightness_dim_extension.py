# ============================================================
# Brightness Dimmer extension for Falkon
# Copyright (C) 2019 Zdravko Mitov <mitovz@mail.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ============================================================
import Falkon
from PySide2 import QtCore
from brightnessdimmer.brightness_dim_button import BrightnessDimButton
from brightnessdimmer.brightness_dim_connection import DBConnection


class BrightnessDim(Falkon.PluginInterface, QtCore.QObject):
    buttons = {}
    db = None

    def init(self, state, settingsPath):
        self.db = DBConnection()
        self.db.db_path = settingsPath

        plugins = Falkon.MainApplication.instance().plugins()

        plugins.mainWindowCreated.connect(self.on_main_window_created)
        plugins.mainWindowDeleted.connect(self.on_main_window_deleted)
        plugins.webPageCreated.connect(self.on_page_created)

        if state == Falkon.PluginInterface.LateInitState:
            for window in Falkon.MainApplication.instance().windows():
                self.on_main_window_created(window)

    def unload(self):
        self.db.db.close()
        del self.db.db

        for window in Falkon.MainApplication.instance().windows():
            self.on_main_window_deleted(window)

    def testPlugin(self):
        return self.db.create_connection()

    def on_main_window_created(self, window):
        button = BrightnessDimButton(self.db)
        window.navigationBar().addToolButton(button)
        self.buttons[window] = button

    def on_main_window_deleted(self, window):
        if window not in self.buttons:
            return
        button = self.buttons[window]
        window.navigationBar().removeToolButton(button)
        del self.buttons[window]

    def on_page_created(self, page):
        page.loadFinished.connect(lambda: self.db.apply_value(page))


Falkon.registerPlugin(BrightnessDim())
